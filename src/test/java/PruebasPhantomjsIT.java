import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votación mejor jugador liga ACB", driver.getTitle(),
                "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void ponerVotosCeroTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement resetVotesButton = driver.findElement(By.name("B3"));
        resetVotesButton.click();

        wait.until(ExpectedConditions.titleIs("Restablecer votos"));
        WebElement link = driver.findElement(By.linkText("Ir al comienzo"));
        link.click();

        wait.until(ExpectedConditions.titleIs("Votación mejor jugador liga ACB"));
        WebElement viewVotesButton = driver.findElement(By.name("B4"));
        viewVotesButton.click();

        wait.until(ExpectedConditions.titleIs("Ver votos"));

        WebElement votaciones = driver.findElement(By.tagName("table"));

        ArrayList<WebElement> rows = new ArrayList<>(votaciones.findElements(By.tagName("tr")));
        rows.remove(0);
        ArrayList<WebElement> listaVotos = new ArrayList<>();

        for (int i = 0; i < rows.size(); i++) {
            List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
            listaVotos.add(cols.get(2));
        }

        boolean isZeroVotes = true;
        int i = 0;

        while (isZeroVotes && i < listaVotos.size()) {
            if (!listaVotos.get(i).getText().trim().equals("0")) {
                isZeroVotes = false;
            }
            i++;
        }

        assertEquals(true, isZeroVotes, "No todos los votos son cero");

        driver.close();
        driver.quit();
    }

    @Test
    public void votarOtroJugador() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebDriverWait wait = new WebDriverWait(driver, 10);

        driver.findElement(By.name("txtOtros")).sendKeys("Hezonja");
        driver.findElement(By.id("otro")).click();

        driver.findElement(By.name("B1")).click();
        wait.until(ExpectedConditions.titleIs("Votación mejor jugador liga ACB"));
        driver.findElement(By.linkText("Ir al comienzo")).click();

        driver.findElement(By.name("B4")).click();
        wait.until(ExpectedConditions.titleIs("Ver votos"));

        WebElement votaciones = driver.findElement(By.tagName("table"));

        ArrayList<WebElement> rows = new ArrayList<>(votaciones.findElements(By.tagName("tr")));
        rows.remove(0);

        boolean isPlayerVoted = false;
        int i = 0;

        while (!isPlayerVoted && i < rows.size()) {
            List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
            if (cols.get(1).getText().trim().equals("Hezonja") && cols.get(2).getText().trim().equals("1")) {
                isPlayerVoted = true;
            }
            i++;
        }

        assertEquals(true, isPlayerVoted, "El jugador no ha sido votado");

        driver.close();
        driver.quit();
    }
}
