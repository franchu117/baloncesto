import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        instance.cerrarConexion();
        assertEquals(expResult, result);
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizar jugador");
        String nombre = "Llull";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        int votos = 0;
        if (instance.existeJugador(nombre)) {
            instance.actualizarJugador(nombre);
            votos = instance.getVotos(nombre);
        } else {
            instance.insertarJugador(nombre);
            instance.actualizarJugador(nombre);
            votos = instance.getVotos(nombre);
        }
        assertEquals(1, votos);
        instance.cerrarConexion();
    }
}