<%@ page import="java.util.ArrayList" %>
<%@ page import="Modelos.Jugador"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Ver votos</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="votaciones">
        <h1>Resultados de la votaci&oacute;n</h1>
        <% ArrayList<Jugador> listaJugadores = (ArrayList<Jugador>) session.getAttribute("listaJugadores");%>
        <table>
            <tr>
                <th>Jugador</th>
                <th>Votos</th>
            </tr>
           
                <% 
                for(Jugador jugador : listaJugadores) { %>
                  <tr>
                    <td><%= jugador.getId() %></td>
                    <td><%= jugador.getNombre() %></td>
                    <td><%= jugador.getVotos() %></td>
                  </tr>
                 <%
                }     
                   %>
        
            </table>
            <br>
        <br> <a href="index.html"> Ir al comienzo</a>
        </body>
    </html>
