
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ResetearVotos extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        bd.resetearVotos();
        // Llamada a la página jsp que nos indica si los votos de los jugadores se han puesto a 0
        res.sendRedirect(res.encodeRedirectURL("ResetVotos.jsp"));
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
